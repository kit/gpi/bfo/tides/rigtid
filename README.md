On the RIGTID - Program
=======================

The program RIGTID together with its subroutines computes the tidal
accelerations on a rigid earth, i. e. no response of the Earth is considered.
The tidal accelerations are calculated directly from the positions of moon and
sun separately, i. e. if wanted two groups of tides (solar or lunar) can be
handled differently. The program does not use a potential development, it
includes the second, third, and fourth degree potential of the moon, but only
the second degree potential of the sun. It treats the earth as an elliptically
shaped body but does not include the direct effect of the earth's ellipticity
on the tidal accelerations (Wilhelm 1983[^5] and Dahlen 1993[^2]).
It also does not take into account the speed of light. No planets are
included, this is discussed in the following paper
([DOI: 10.1029/GM016p0319](http://dx.doi.org/10.1029/GM016p0319)):  

    Broucke, R. A., Zuern, W. E., Slichter, L. B., 1972
    The Lunar Tidal Acceleration on a Rigid Earth.
    in "Geophysical Monographs 16": Flow and Fracture of Rocks" 
    (The Griggs Volume, Eds. Heard, H. C., Borg, I. Y., Carter, N. L.,
    Raleigh, C. B.), Am. Geophys. Union, Washington, 319 - 324.
    DOI: 10.1029/GM016p0319

This program was developed at UCLA in 1971/72 by Walter Zuern with the
help of subroutines provided by Roger Broucke from JPL describing the
position of the moon much more accurately than is done by the program
developed by Longman previously. The description of the celestial
mechanics of the Earth-Moon-Sun system was still very accurate in 2017
when compared to ephemerides, which are the basis of the most modern
tidal potential catalogues.

The program was originally already intended for free distribution
("open source" in 2017 speak). It has happened before that
modifications were made in the main program by some people (rigtid.f)
which were later detected to be faulty. It is recommended that users
track the path of their version either starting at UCLA (probably not
possible any more) or at BFO in order to be sure that no unwanted
modifications crept in. If the program is used and an author wants a
reference, the above (short) publication is the only one available.   

24th November 2017   
Walter Zuern

Black Forest Observatory (BFO, Schiltach)   
Heubach 206, 77709 Wolfach, Germany   
walter.zuern@partner.kit.edu   

Source code files of rigtid
---------------------------

main programs:

    gezeitn3.f    main driver program
    benchmrk.f    driver for benchmark case

subroutines:

    arc.f         compute angle from sin and cos
    crmoon.f      compute apparent geocentric coordinates of the moon
    etmutc.f      compute ephemeris time from UTC
    gstime.f      compute greenwich siderial time from universal time
    lunar.f       compute geocentric coordinates of the moon
    modulo.f      modulo function (utility subroutine)
    nutan.f       compute nutation in longitude and obliquity
    rigtid.f      compute tidal acceleration of sun and moon
    sun.f         compute sun earth ephemeris

supporting files:

    benchmrk.inp  input file to benchmrk.f
    benchmrk.ref  expected output of benchmrk.f  
                  These values were last checked by Walter Zürn
                  against reference solutions in 2010 and 2017.
                  See also comments in the source code benchmrk.f

Running rigtid and working examples
-----------------------------------
The program `gezeitn3.f` computes a time series of rigid earth tides.
Upon execution it reads control parameters from a file with name `gezeitn3.inp`.
The time series samples are output to a file named `gezeitn3.out`.
The format of the control parameter file is defined in the header comments of `gezeitn3.f`:

| line | variable        | format         | meaning of parameter                       |
| ---: | :-------------- | :------------- |:------------------------------------------ |
| 1    | OL, OM          | (2F8.4)        | STATION LONGITUDE IN DEG., MIN (EAST -)    | 
| 2    | AL, AM          | (2F8.4)        | STATION LATITUDE  IN DEG., MIN (SOUTH -)   | 
| 3    | HT,DELH,DELM    | (F8.3,2F8.3)   | HT = STATION HEIGHT    IN METERS           |
|      |                 |                | DEL = TIMESTEP(HOURS,MINUTES RESPECTIVELY) |
| 4    | ATAG,AJAHR      | (2I5)          | START: DAY OF YEAR,YEAR                    |
| 5    | STHR,STMN,KLOOP | (2F8.3,4X,I10) | START: HOUR,MINUTE/TOTAL NUMBER OF POINTS  |
| 6    | S1, KORR      | (2X,I2,2X,F10.5) | S1 = KOMPONENT:                            |
|      |                 |                | 0 - VERTICAL                               |
|      |                 |                | 1 - HORIZ.(SOUTH)                          |
|      |                 |                | 2 - HORIZ.(EAST)                           |
|      |                 |                | 3 - HORIZON.(N DAZ E)                      |
|      |                 |                | KORR = ARBITRARY FACTOR                    |
| 7    | DAZ             | (F8.3)         | AZIMUTH for tilt, measured cw from N       |

### Sign convention
Latitude is positive to the North and negative to the South.
Longitude is positive to the West and negative to the East.

Degree and minute part are added with sign to compute the respective
latitude or longitude. Hence, usually the minute part should be negative as
well in cases where the degree part is negative.

### Leap seconds
Leap seconds are taken into account in [etmutc.f](etmutc.f).
The table in this file might not be up-to-date. 
Please check this table against data provided by the 
[IERS Earth Orientation Centre](http://hpiers.obspm.fr/eop-pc/).

### Examples

Some examples of control files are distirbuted together with the code.
Rename the respective example to `gezeitn3.inp` and run the program.

| filename       | example               |
| :------------- | :-------------------- |
| `ewtilt.inp`   | EW-tilt               |
| `y2k.inp`      | verify Y2K compliance |
| `spa.inp`      | gravity at South Pole |
| `hannover.inp` | gravity at Hannover   |

Accuracy and accuracy tests
---------------------------

### Improvements after 1973
1. Geographics latitudes are converted to geocentric latitude before
   calculations.
2. The components are calculated along the coordinate system aligned
   with the plumb-line at the station.
3. The ephemeris of the sun has been improved.

The only valid comparison with values published by Broucke et al.
(1972)[^1] can be made for the lunar accelerations at the poles.

### Accuracy test in 2010
Walter Zürn tested the program against predict (Eterna 3.30) in 2010.
Residuals to results computed with Eterna using the tidal catalogue of
Hartmann and Wenzel (1995)[^3] then were smaller than the residuals of the
Eterna results for the catalogue of Tamura (1987)[^4] with respect to the
results for the catalogue of Hartmann and Wenzel (1995)[^3].

At the same time Walter Zürn checked the accuracy of the results of the
benchmark-case in benchmrk.ref against reference values. He confirmed the
purely numerical accuracy of the computation to be better than 1 nGal.
This does consider the accuracy with respect to actual tidal forces, which 
may suffer from the missing effect of planets and tidal potential of the moon
being considered only up to 4th degree in rigtid.

### Accuracy test in 2017
While preparing the public release of rigtid, Walter Zürn and Thomas Forbriger
verified the reference solutions as provided in file
[benchmrk.ref](benchmrk.ref). 
See messages to commits c5b041af2 and 4fab155fd.

References
----------
[^1]: Broucke, R. A., Zuern, W. E., Slichter, L. B., 1972.
  The Lunar Tidal Acceleration on a Rigid Earth.
  in ''Geophysical Monographs 16: Flow and Fracture of Rocks'' 
  (The Griggs Volume, Eds. Heard, H. C., Borg, I. Y., Carter, N. L.,
  Raleigh, C. B.), Am. Geophys. Union, Washington, 319 - 324.
  [DOI: 10.1029/GM016p0319](http://dx.doi.org/10.1029/GM016p0319)

[^2]: Dahlen, F. A., 1993.
  Effect of the Earth’s ellipticity on the lunar tidal potential.
  Geophys. J . Int. vol. 113(1), pp. 250-251.
  [DOI: 10.1111/j.1365-246X.1993.tb02543.x](http://dx.doi.org/10.1111/j.1365-246X.1993.tb02543.x)

[^3]: Hartmann, T. and H.-G. Wenzel, 1995.
  The HW95 tidal potential catalogue. Geophysical Research Letters.
  vol. 22 no. 24, pp. 3553–3556. 
  [DOI: 10.1029/95GL03324](http://dx.doi.org/10.1029/95GL03324)

[^4]: Tamura, Y, 1987.
  A harmonic development of the tide-generating potential.
  Bull. Inf. Marées Terrestres. vol. 99, pp. 6813–6855.
  http://www.bim-icet.org/

[^5]: Wilhelm, H, 1983.
  Earth's Flatteing Effect on the Tidal Forcing Field.
  J. Geophys. vol. 52(2), pp. 131-135. 
