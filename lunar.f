c this is <lunar.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 1972 by R. A. Broucke (JPL)
c
c compute geocentric coordinates of the moon
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c ============================================================================
c
      SUBROUTINE lunar(DATE,A,B,C)                                      
COMPUTES GEOCENTRIC COORDINATES OF THE MOON WITH BROWN'S LUNAR THEORY   
C  PRECISION=  2.SEC OF ARC IN LONGITUDE                                
C              1. SEC OF ARC IN LATITUDE                                
C              .1 SEC OF ARC IN PARALLAXE                               
C  INPUT =  DATE =JULIAN DAY IN EPHEMERIS TIME (DOUBLE PRECISION)       
C  OUTPUT= CLONG = LONGITUDE                                            
C          BETA  = LATITUDE                                             
C          PAR   = PARALLAX                                             
C ALL OUTPUT IS IN RADIANS  W.R. TO MEAN EQUINOXE OF DATE               
C TO COMPUTE EARTH-MOON DISTANCE, USE                                   
C            R =(EARTH  EQUATORIAL  RADIUS)/(SINE OF PARALLAX)          
      DIMENSION ANGLE(5),IX(4,61),X(61),IY(4,48),Y(48),IZ(4,30),Z(30),  
     1 EN(10),IN(4,10),EG(13),IG(4,13),ADD(3)                           
      DOUBLE PRECISION DATE,DD,DD2,DD3,PI,RASEC,RADIAN,ANGLE,ADD,A,B,C,D
C  SUPPLY DATE (JULIAN DATE +DECIMAL OF DAY)                            
      DATA IX/ 1,0,0,0, 1,0,0,-2, 0,0,0,2, 2,0,0,0, 0,+1,0,0, 0,0,2,0, 2
     1,0,0,-2, 1,1,0,-2, 1,0,0,2, 0,1,0,-2, 1,-1,0,0, 0,0,0,1, 1,1,0,0, 
     20,0,2,-2, 1,0,2,0, 1,0,-2,0, 1,0,0,-4, 3,0,0,0, 2,0,0,-4, 1,-1,0,-
     32, 0,1,0,2, 1,0,0,-1, 0,1,0,1, 1,-1,0,2, 2,0,0,2, 0,0,0,4, 3,0,0,-
     42,2,-1,0,0, 1,0,-2,-2, 2,1,0,-2, 1,0,0,1, 0,2,0,-2, 2,1,0,0,0,2,0,
     50, 1,2,0,-2, 1,0,-2,2, 0,0,2,2, 1,1,0,-4, 2,0,2,0,1,0,0,-3, 1,1,0,
     6 2, 2,1,0,-4, 2,-1,0,-2, 1,-2,0,0, 1,-2,0,-2, 0,1,2,-2, 1,0,0,4,  
     7 4,0,0,0, 0,1,0,-4, 2,0,0,-1, 0,1,-2,2, 2,0,-2,0, 1,1,0,1, 2,0,0, 
     8   -3, 3,0,0,-4, 2,-1,0,2, 1,2,0,0, 1,-1,0,-1, 3,0,0,2, 1,0,2,2, 4
     9,0,0,-2 /                                                         
      DATA IY/1,0,0,0, 1,0,0,-2, 0,0,0,2, 2,0,0,0, 1,0,0,2, 1,1,0,-2, 0,
     11,0,-2, 2,0,0,-2, -1,1,0,0, 0,1,0,0, 1,1,0,0, 0,0,0,1, -1,0,2,0, 0
     2,0,2,-2, 3,0,0,0, 1,0,0,-4, 2,0,0,-4,-1,1,0,-2, 0,1,0,2,-1,1,0,+2,
     3 0,1,0,1,  3,0,0,-2, 0,2,0,-2, 2,0,0,2, 0,0,0,4, 1,0,0,1, 1,1,0,2,
     4 2,-1,0,0, 2,1,0,0, 1,1,0,-4, 1,0,2,-2, 2,1,0,-2, 1,0,0,4, 0,1,0,-
     5 4, 1,2,0,-2, 1,0,0,-3, 0,0,0,3,  4,0,0,0, 1,0,0,-1, -1,0,2,-2,   
     6 2,-1,0,2, 3,0,0,2, 2,1,0,-4, -1,2,0,2, -1,2,0,0, -1,1,0,4, 2,0,0,
     7 -1, -1,2,0,-2 /                                                  
      DATA IZ /1,0,0,0, 1,0,0,-2, 0,0,0,2, 2,0,0,0, 1,0,0,2, 0,1,0,-2, 1
     1,1,0,-2, 1,-1,0,0, 0,0,0,1, 1,1,0,0 , 1,0,-2,0, 3,0,0,0, 1,0,0,-4,
     2 0,1,0,0, 2,0,0,-4, 0,1,0,2, 2,0,0,-2, 2,0,0,2, 0,0,0,4, 1,-1,0,2,
     3 1,-1,0,-2, 0,1,0,1, 2,-1,0,0, 3,0,0,-2, 1,0,0,1, 0,0,2,-2, 2,1,0,
     40, 0,2,0,-2, 1,0,2, -2,  1,1,0,-4/                                
      DATA IN / 0,0,1,-2, 1,0,1,-2, -1,0,1,-2,-2,0,1,0, 0,1,1,-2, -1,0,1
     1,0,0,-1,1,-2,1,0,1,-4,0,0,1,-4,-2,0,1,-2/                         
      DATA IG /2,0,0,-2, 1,1,0,-2, -1,1,0,-2, 3,0,0,0, 0,1,0,0, 1,1,0,2,
     1 0,0,0,1, 0,2,0,-2, 0,0,0,2, 1,1,0,-4, 1,0,0,4, 1,0,0,1,          
     2 -1,1,0,2/                                                        
      DATA X /22639.6, -4586.5, 2369.9, 769.0, -668.1, -411.6, -211.6, -
     1206.0, 192.0, -165.1, 147.7, -125.2, -109.7, -55.2, -45.1, +39.5, 
     2-38.4, +36.1, -30.8, +28.5, -24.4, +18.6, 18.0, 14.6, 14.4, 13.9, 
     3-13.2, 9.7, 9.4, -8.6, -8.5, -8.1, -7.6, -7.5, -7.4, -6.4, -5.7, -
     44.4, -4.0,+3.2,-2.9,-2.7,-2.5,+2.6,+2.5,-2.1,+2.0,+1.9,-1.8,+1.8,-
     5 1.4,-1.3,+1.3,+1.2,-1.1,+1.2,-1.1,-1.1,+1.0,-1.0,-0.9 /          
      DATA  Y/ 22609., -4578.1, 2373.4, 768., 192.7, -182.4, -165., -152
     1.5, -138.8, -127.0, -115.2, -112.8, -85.1, -52.1, 50.6, -38.6, -34
     2.1, -31.7, -25.1, -23.6, 17.9, -16.4, -16.4,+14.78,+14.06,-13.51,-
     311.75,+11.67,-10.56,-9.66,-9.52,-7.59,+6.98,-6.46,-6.12,+5.44,-4.0
     41,+3.60,+3.59,+3.37,+3.32,+2.96,-2.54,-2.40,-2.32,-2.27,+2.01,-1.8
     52 /                                                               
      DATA Z /           +186.5, 34.3, 28.2, 10.2, 3.1, 1.9, 1.5, 1.2, -
     11.0, -0.9,-.71,+.62,+.60,-.40,+.37,-.30, -.30,+.28, +.26, +.23, -.
     2 23, +.15, +.13, -.12, -.11,-.11,-.10,+.09,-.08,+.07 /            
      DATA EN /-526.0,44.3,-30.6,-24.6,-22.6,20.6,11.0,-6.0,-3.3        
     1,-2.0/                                                            
      DATA EG/5.7,2.1,-1.5,-1.3,-1.3,0.8,-0.7,-0.7,0.6,-0.5,-0.4,0.4,-0.
     14/                                                                
      PI = 3.141592653589D0                                             
      TWOPI = 2.*PI                                                     
      RADIAN = 180.D0/PI                                                
      RASEC = PI/(180.D0*3600.D0)                                       
      D = DATE - 2415020.D0                                             
      DD=D/10000.0D0                                                    
      DD2 = DD*DD                                                       
      DD3 = DD2*DD                                                      
      ANGLE(1)=270.434164D0 +13.1763965268D0*D -.0000850*DD2            
     1 +.000000039D0*DD3                                                
      ANGLE(2) = 279.696678D0 +.9856473354D0*D+.00002267*DD2            
      ANGLE(3) = 334.329556D0 +.1114040803D0*D - .0007739*DD2           
     1 -.00000026D0*DD3                                                 
      ANGLE(4) = 259.183275D0 -.0529539222D0*D +.0001557*DD2            
     1 -.00000005*DD3                                                   
      ANGLE(5) = 281.220833D0 +.0000470684D0*D +.0000339*DD2            
     1 +.00000007*DD3                                                   
C ANGLE 1 IS MEAN LONGITUDE OF MOON AT EPOCH                            
C ANGLE 2 IS MEAN LONGITUDE OF SUN AT EPOCH                             
C ANGLE 3 IS LONGITUDE OF LUNAR PERIGEE AT EPOCH                        
C ANGLE 4 IS LONGITUDE OF ASCENDING NODE AT EPOCH                       
C ANGLE 5 IS LONGITUDE OF SOLAR PERIGEE AT EPOCH                        
      DO 01 I = 1,5                                                     
      J = ANGLE(I)/360.D0                                               
      ANGLE(I) = (ANGLE(I) - 360*J)/RADIAN                              
   01 IF ( ANGLE(I).LT.0.0) ANGLE(I) = ANGLE(I)+2.*PI                   
      ADD(1) = 2.*PI*(.14222222D0  + .000001536238D0*D)                 
      ADD(2) = 2.*PI*(.48398132D0   -.000147269147D0*D)                 
      ADD(3) = 2.*PI*(.53733431D0 -.000010104982D0*D)                   
      EEL=ANGLE(1)+RASEC*(14.27*DSIN(ADD(3))+7.26*DSIN(ANGLE(4))+.84*DSI
     1N(ADD(1)))                                                        
C TWO ADDITIVE TERMS IN ANODE (BELOW) ARE ADDED AS ROUGH APPROXIMATION  
      ANODE=ANGLE(4)+RASEC*(96.*DSIN(ANGLE(4))+(15.6+1.9)*DSIN(ADD(2))) 
      OMOON=ANGLE(3)+RASEC*(-2.10*DSIN(ADD(1))-2.08* SIN(ANODE)-.84*DSIN
     1(ADD(2)))                                                         
      EL = EEL - OMOON                                                  
      ELP = ANGLE(2) - ANGLE(5)                                         
      F = EEL - ANODE                                                   
      DEE = EEL - ANGLE(2)                                              
      CLONG = EEL                                                       
      S = F                                                             
      PAR = 3422.7*RASEC                                                
      DO  11  K=1,61                                                    
   11 CLONG=CLONG    + X(K)*RASEC*SIN(IX(1,K)*EL +IX(2,K)*ELP + IX(3,K) 
     1*F + IX(4,K)*DEE )                                                
      IF (CLONG.GE.TWOPI) CLONG = CLONG - TWOPI                         
      IF (CLONG.LT. 0.0) CLONG = CLONG + TWOPI                          
      DO  12  K=1,48                                                    
   12 S = S +          Y(K)*RASEC*SIN(IY(1,K)*EL +IY(2,K)*ELP + IY(3,K) 
     1*F + IY(4,K)*DEE )                                                
      SCREW = (1. -.0004664*COS(ANODE) -.0000754*COS(ANODE+4.82))*SIN(S)
      BETA  = (18519.7*SCREW -6.2*SIN(3.*S))*RASEC                      
      DO  13  K=1,30                                                    
   13 PAR    = PAR    +Z(K)*RASEC*COS(IZ(1,K)*EL +IZ(2,K)*ELP + IZ(3,K) 
     1*F + IZ(4,K)*DEE )                                                
      DO  14  K=1,10                                                    
   14 BETA   =BETA   +EN(K)*RASEC*SIN(IN(1,K)*EL +IN(2,K)*ELP + IN(3,K) 
     1*F + IN(4,K)*DEE )                                                
      G1C = 0.                                                          
      DO  15  K=1,13                                                    
   15 G1C = G1C+RASEC*EG(K)*COS(IG(1,K)*EL +IG(2,K)*ELP +IG(3,K)*F +IG(4
     1,K)*DEE)                                                          
      BETA = BETA + G1C*SIN(S)                                          
      A = CLONG                                                         
      B = BETA                                                          
      C = PAR                                                           
      RETURN                                                            
      END             
                                                  
c
c ----- END OF lunar.f ----- 
