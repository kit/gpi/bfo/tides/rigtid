c this is <crmoon.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 1972 by R. A. Broucke (JPL)
c
c compute apparent geocentric coordinates of the moon
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c ============================================================================
c
      SUBROUTINE CRMOON(DATE,COODEK,COORAS,COOPAR)                      
C                                                                       
C COMPUTES APPARENT GEOCENTRIC COORDINATES OF THE MOON .                
C                                                                       
C     COOR(1)=LONGITUDE OF MOON(RADIANS,MEAN EQUINOX OF DATE)           
C     COOR(2)=LATITUDE  OF MOON(RADIANS,MEAN EQUINOX OF DATE)           
C     COOR(3)=PARALLAX  OF MOON(RADIANS,MEAN EQUINOX OF DATE)           
C     COOR(4)=NUTATION CORRECTION IN LONGITUDE (RADIANS)                
C     COOR(5)=NUTATION CORRECTION IN OBLIQUITY (RADIANS)                
C     COOR(L)=LONGITUDE OF MOON (RADIANS,TRUE EQUINOX OF DATE)          
C     COOR(7)=MEAN OBLIQUITY OF ECLIPTIC (RADIANS)                      
C     COOR(8)=TRUE OBLIQUITY OF ECLIPTIC (RADIANS)                      
C     COOR(9,10,11)=X,Y,Z=RECTANGULAR ECLIPTIC COORDINATES OF MOON      
C                   WITH TRUE ECLIPTIC AND EQUINOX OF DATE              
C                   THE UNITS ARE EARTH RADII                           
C     COOR(12,13,14)=X,Y,Z=RECTANGULAR EQUATORIAL COORDINATES OF MOON   
C                   IN EARTH RADII,WITH TRUE EQUATOR OF DATE            
C     COOR(15)=TRUE  RIGHT ASCENSION OF MOON (RADIANS,EQUAT.OF DATE)    
C     COOR(16)=TRUE  DECLINATION OF MOON (RADIANS,EQUATOR OF DATE)      
      DOUBLE  PRECISION DATE,COOR(16),RAD,T,SINP,COSL,SINL,COSB,SINB,   
     *COSE,SINE                                                         
      DOUBLE PRECISION  COODEK,COORAS,COOPAR                            
      DATA RAD/.1745329251994330D-1/                                    
      CALL LUNAR(DATE,COOR(1),COOR(2),COOR(3))                          
      CALL NUTAN(DATE,COOR(4),COOR(5))                                  
      COOR(6)=COOR(1)+COOR(4)                                           
      T=(DATE-2415020.0D0)/36524.21988D0                                
C     T=TIME IN TROPICAL CENTURIES FROM 1900                            
      COOR(7)=(23.452294D0-.0130125D0*T-.164D-5*T**2+.503D-6*T**3)*RAD  
      COOR(8)=COOR(7)+COOR(5)                                           
      SINP=DSIN(COOR(3))                                                
      COSL=DCOS(COOR(6))                                                
      SINL=DSIN(COOR(6))                                                
      COSB=DCOS(COOR(2))                                                
      SINB=DSIN(COOR(2))                                                
      COOR(09)=COSL*COSB/SINP                                           
      COOR(10)=SINL*COSB/SINP                                           
      COOR(11)=SINB     /SINP                                           
      COSE=DCOS(COOR(8))                                                
      SINE=DSIN(COOR(8))                                                
      COOR(12)=COOR(09)                                                 
      COOR(13)=COOR(10)*COSE-COOR(11)*SINE                              
      COOR(14)=COOR(10)*SINE+COOR(11)*COSE                              
      CALL ARC(COOR(12),COOR(13),COOR(15))                              
      COOR(16)=COOR(12)**2+COOR(13)**2                                  
      COOR(16)=DSQRT(COOR(16))                                          
      COOR(16)=DATAN(COOR(14)/COOR(16))                                 
       COODEK=COOR(16)                                                  
      COORAS=COOR(15)                                                   
      COOPAR=COOR(3)                                                    
      RETURN                                                            
      END                                                              
c ----- END OF crmoon.f ----- 
