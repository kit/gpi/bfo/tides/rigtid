c this is <gezeitn3.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 1972 by Walter Zuern (UCLA)
c
c main driver program
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c ============================================================================
c
C***********************************************************************
C     TIDAL ACCELERATION OF RIGID EARTH                                 
C            VERSION FOR EQUIDISTANT TIME SERIES WITH ARBITRARY START TIME
C***********************************************************************
C     INPUT(FILE#5) :                                                   
C 1)  OL, OM       (2F8.4)    STATION LONGITUDE IN DEG., MIN (EAST -)   
C 2)  AL, AM       (2F8.4)    STATION LATITUDE  IN DEG., MIN (SOUTH -)  
C 3)  HT           (F8.3)     STATION HEIGHT    IN METERS               
C     DELH,DELM    (2F8.3)    TIMESTEP(HOURS,MINUTES RESPECTIVELY)      
C 4)  ATAG,AJAHR (2I5)        START: DAY OF YEAR,YEAR
C 5)  STHR,STMN,KLOOP (2F8.3,4X,I10)
C                             START: HOUR,MINUTE/TOTAL NUMBER OF POINTS
C 6)  S1, KORR     (2X,I2,2X,F10.5) S1 = KOMPONENT : 0 - VERTICAL       
C                                                  1 - HORIZ.(SOUTH)    
C                                                2 - HORIZ.(EAST)       
C                                              3 - HORIZON.(N DAZ E)
C                                KORR = ARBITRARY FACTOR                
C 7)  DAZ            (f8.3)   Azimut fuer tilt,measured cw from N
C                                                                      
C***********************************************************************                    
      IMPLICIT REAL*8(A-H,O-Z)
      INTEGER MSAMP
      PARAMETER(MSAMP=1000000)
      DIMENSION G(MSAMP)
      INTEGER ATAG,AJAHR,S1
      DOUBLE PRECISION HTD,ALD,AMD,OLD,OMD,yrsd,hrsdd,daysrd,gs,gm,
     1 daysd,t,tujd,tejd,hem,hsm,hes,hss,delh,delm,dstep,si
      REAL KORR                                                         
C***** INPUT + FEEDBACK *********************************************** 
      open(UNIT=5,FILE='gezeitn3.inp')
      OPEN(UNIT=8,FILE='gezeitn3.out')
      READ(5,101)OL,OM                                                  
      READ(5,101)AL,AM                                                  
  101 FORMAT(2F8.3)                                                     
      READ(5,102) HT,DELH,DELM                                          
  102 FORMAT(3F8.3)                                                     
      READ(5,103) ATAG,AJAHR
      READ(5,131) STHR,STMN,KLOOP                                       
  131 FORMAT(2f8.3,4x,i10)
  103 FORMAT(2I5)
      READ(5,104) S1,KORR                                               
  104 FORMAT(2X,I2,2X,F10.5)                                            
      read(5,401) DAZ
  401 format(f8.3)
      dga=daz*atan(1.000)/45.000
      IF (KLOOP.GT.MSAMP) THEN
        PRINT *,'ERROR: ',KLOOP,' SAMPLES ARE REQUESTED'
        PRINT *,'ARRAY IN PROGRAM IS PREPARED FOR ',
     1            MSAMP,' SAMPLES ONLY'
        PRINT *,'ADJUST ARRAY SIZE OR CONTROL PARAMETER IN gezeitn3.inp'
        STOP 'ABORT'
      ENDIF
      IF(S1.EQ.0) WRITE(8,105)                                          
      IF(S1.EQ.1) WRITE(8,106)                                          
      IF(S1.EQ.2) WRITE(8,107)                                          
      if(s1.eq.3) write(8,133)
  105 FORMAT(2X,'COMPUTATION OF VERTICAL TIDAL ACCELERATION')           
  106 FORMAT(2X,'COMPUTATION OF HORIZONTAL(SOUTH) TIDAL ACCELERATION')  
  107 FORMAT(2X,'COMPUTATION OF HORIZONTAL(EAST) TIDAL ACCELERATION')   
  133 Format(2x,'computation of horiz. (N daz E) tidal acceleration')
      WRITE(8,108)OL,OM                                                 
  108 FORMAT(2X,'STATION LONGITUDE:',2X,F8.3,' DEG',F8.3,1H')             
      WRITE(8,109)AL,AM                                                 
  109 FORMAT(2X,'STATION LATITUDE:',3X,F8.3,' DEG',F8.3,1H')              
      WRITE(8,110) HT,DELH,DELM                                         
  110 FORMAT(2X,'STATION HEIGHT:',5X,F8.3,'M',5X,'TIMESTEP: ',F8.3,'H',    
     1F8.3,'MIN')                                                       
      WRITE(8,111) ATAG,AJAHR
  111 FORMAT(2X,'START  DAY : ',I5,'  OF YEAR:',I5)
      WRITE(8,132) STHR,STMN
  132 FORMAT(2x,' HOURS:',f8.3,'   MINUTES:  ',f8.3)
      write(8,134) daz
  134 Format(2x,'azimuth = N',f8.3,'  E')
      htd=dble(ht)
      ald=dble(al)
      amd=dble(am)
      old=dble(ol)
      omd=dble(om)
C***********************************************************************
      CALL rigtid(HTD,ALD,AMD,OLD,OMD)
C***********************************************************************
      YRS= FLOAT(AJAHR-1900)                                            
   51 DAYSR= FLOAT(ATAG)
C********************************************************************** 
      K=1                                                               
      DSTEP=DELH+DELM/60.                                               
      SI=-DSTEP +sthr+stmn/60.00                                        
   56 IF(K.GT.KLOOP) GOTO 59
   58 SI=SI+DSTEP                                                       
      HRSD=SI                                                           
      yrsd=dble(yrs)
      daysrd=dble(daysr)
      hrsdd=dble(hrsd)
      CALL rigtim(YRSD,DAYSRD,HRSDD,GS,GM,DAYSD,T,TUJD,TEJD,HEM,HSM,    
     1HES,HSS)                                                          
      gss=sngl(gs)
      gms=sngl(gm)
      hems=sngl(hem)
      hsms=sngl(hsm)
      hess=sngl(hes)
      hsss=sngl(hss)
      IF(S1.EQ.0) G(K)=(GSS +GMS )*KORR                                 
      IF(S1.EQ.1) G(K)=(HSSS+HSMS)*KORR                                 
      IF(S1.EQ.2) G(K)=(HESS+HEMS)*KORR                                 
      if(s1.eq.3) g(k)=korr*(sin(dga)*(hess+hems)-cos(dga)*(hsss+hsms))
      K=K+1                                                             
      if(HRSD.LT.24.00)  GOTO 56
      SI=SI-24.000
      DAYSR=DAYSR+1.000
      GOTO 56
   59 CONTINUE                                                          
C***** OUTPUT ******************************************************    
      WRITE(8,124)KLOOP                                                 
  124 FORMAT(2X,'NUMBER OF VALUES   :',I8)
      if(s1.ne.0)  go to 98
C convert microgals to nm/s/s
      DO 300 K=1,KLOOP                                                
      G(K)=G(K)*10.                                                   
  300 CONTINUE                                                          
      WRITE(8,122)KORR                                                  
  122 FORMAT(2X,'ALL VALUES IN  (MYCGAL*E-1)  * ',F10.5)                
      WRITE(8,121) (G(j),j=1,kloop) 
  121 FORMAT(F10.4)
  200 CONTINUE                                                          
      go to 99
   98 continue
c convert microgals to mseca/10
      do 301 k=1,kloop
      g(k) = g(k)/0.981*1.8*0.9/atan(1.000)
  301 continue
      write(8,135) korr
  135 format(2x,'all values in (MSECARC*E-1) *',f10.5)
      write(8,121) (g(j),j=1,kloop)
   99 STOP                                                              
      end
      include 'rigtid.f'
      include 'sun.f'
      include 'modulo.f'
      include 'crmoon.f'
      include 'lunar.f'
      include 'nutan.f'
      include 'arc.f'
      include 'gstime.f'
      include 'etmutc.f'

c
c ----- END OF gezeitn3.f ----- 
