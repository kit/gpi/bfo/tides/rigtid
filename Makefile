# this is <Makefile>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2017 by Thomas Forbriger (BFO Schiltach) 
# 
# compile rigtid and run test case
#
# ----
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ----
#
# to compile programs run
#
#   make all
#
# to check against reference solution run
#
#   make test
# 
# REVISIONS and CHANGES 
#    12/12/2017   V1.0   Thomas Forbriger
# 
# ============================================================================
#
PROGRAMS=benchmrk gezeitn3

.PHONY: all
all:  $(PROGRAMS)

flist: Makefile $(wildcard *.f README.md COPYING benchmrk.??? *.inp)
	echo $^ | tr ' ' '\n' | sort > $@

.PHONY: edit
edit: flist; vim $<

.PHONY: clean
clean: ; 
	-find . -name \*.bak | xargs --no-run-if-empty /bin/rm -v
	-/bin/rm -vf flist *.o $(PROGRAMS)

SRC= arc.f crmoon.f etmutc.f gstime.f lunar.f modulo.f nutan.f rigtid.f sun.f

# flags for gfortran (GNU compiler)
FFLAGS=-ffixed-line-length-none -std=legacy -fno-automatic
FC=gfortran

benchmrk gezeitn3: %: %.f $(SRC)
	$(FC) $(FFLAGS) -o $@ $<

# test against reference solution
.PHONY: test
test: benchmrk
	@echo run program benchmrk
	./benchmrk
	@echo compare output against reference
	diff -s benchmrk.out benchmrk.ref

# ----- END OF Makefile ----- 
