c this is <benchmrk.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 1972 by Walter Zuern (UCLA)
c
c driver for benchmark case
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c This program reads coordinates from the input file benchmrk.inp and
c computes tidal acceleration for these locations on NOV 11, 22 h, 25 m,
c 37.128 s Universal Time. The coordinates given in benchmrk.inp are the
c same as used in the paper by Broucke et al. (1972). However their
c results differ at a level of at least 0.01 uGal from those obtained by
c rigtid. This is due to improvements applied to the source code after
c 1973:
c
c 1) Geographics latitudes are converted to geocentric latitude before
c    calculations.
c 2) The components are calculated along the coordinate system aligned
c    with the plumb-line at the station.
c 3) The ephemeris of the sun has been improved.
c
c The only valid comparison with values published by Broucke et al.
c (1972) can be made for the lunar accelerations at the poles.
c
c ============================================================================
c
c Revisions and Changes
c   14/12/2017    Thomas Forbriger      
c                   change output format: explain meaning of values,
c                   truncate to reasonable number of digits
c
C***********************************************************************
C     TIDAL ACCELERATION OF RIGID EARTH                                 
C            VERSION FOR BENCHMARK VALUES FOR 1 INSTANT AND 14 LOCATIONS
C***********************************************************************
C     INPUT(FILE#5) :                                                   
C 1)  OL, OM       (2F8.3)    STATION LONGITUDE IN DEG., MIN (EAST -)   
C 2)  AL, AM       (2F8.3)    STATION LATITUDE  IN DEG., MIN (SOUTH -)  
C 3)  HT           (F8.3)     STATION HEIGHT    IN METERS               
C      
C************************************************************************
      DOUBLE PRECISION HT,AL,AM,OL,OM,yrs,hrsd,daysr,gs,gm,hem,hsm,hes,
     1 daysd,t,tujd,tejd,hss
      open(UNIT=5,FILE='benchmrk.inp')
      OPEN(UNIT=8,FILE='benchmrk.out')
      ht = 0.0d0
      om = 0.0d0
      am = 0.0d0
      yrs = 70.0d0
      daysr = 1.0d0
      hrsd = 7558.42698d0
c This is NOV 11, 22 h, 25 m, 37.128 s Universal Time
      write(8, 103) 'NOV 11, 22 h, 25 m, 37.128 s Universal Time',
     &              hrsd, daysr, yrs
      write(8, 104) 'yrs', 'last two digits of year in 20th century'
      write(8, 105) 'i.e. the year 2000 must be given by 100'
      write(8, 104) 'daysr','day of the year'
      write(8, 104) 'hrsd',
     &              'hours and fraction of hours in Greenwich Mean Time'
      write(8, '(/2x,a)') 'Output values:'
      write(8, 104) 'ol','Station W-Longitude / degrees'
      write(8, 104) 'om','Station W-Longitude / minutes'
      write(8, 104) 'al','Station N-Latitude / degrees'
      write(8, 104) 'am','Station N-Latitude / minutes'
      write(8, '(4x,a)') 
     &  'Components of the tidal acceleration of the Sun:'
      write(8, 104) 'gs','vertical / microGal'
      write(8, 104) 'hes','to east / microGal'
      write(8, 104) 'hss','to south / microGal'
      write(8, '(4x,a)') 
     &  'Components of the tidal acceleration of the Moon:'
      write(8, 104) 'gm','vertical / microGal'
      write(8, 104) 'hem','to east / microGal'
      write(8, 104) 'hsm','to south / microGal'
      do 1 i = 1,14
      Read(5,100) ol, al
  100 format(2d15.7)
      write(8,101) i,'ol',ol,'om',om,'al',al,'am',am
C***********************************************************************
      CALL RIGTID(HT,AL,AM,OL,OM)
C***********************************************************************
      CALL RIGTIM(YRS,DAYSR,HRSD,GS,GM,DAYSD,T,TUJD,TEJD,HEM,HSM,    
     1HES,HSS)                                                          
C***********************************************************************
      write(8,102) 'gs',gs,'hes',hes,'hss',hss,
     &             'gm',gm,'hem',hem,'hsm',hsm
    1 continue
   99 STOP                                                              
  101 format(/1x,'Test location number ',i3/
     &       (2x,a4,'=',f12.7,a4,'=',f10.6))
  102 format((2x,3(a4,'=',f12.6)))
  103 format('benchmrk: compute reference solutions for rigtid',/,
     &       10x,'compute for ',a,/,
     &       10x,'hrsd=',f10.5,' daysr=',f10.5,' yrs=',f10.5)
  104 format(a8,': ',a)
  105 format(10x,a)
      END                                                               
      INCLUDE 'rigtid.f'
      INCLUDE 'sun.f'
      INCLUDE 'modulo.f'
      INCLUDE 'crmoon.f'
      INCLUDE 'lunar.f'
      INCLUDE 'nutan.f'
      INCLUDE 'arc.f'
      INCLUDE 'gstime.f'
      INCLUDE 'etmutc.f'
c
c ----- END OF benchmrk.f ----- 
