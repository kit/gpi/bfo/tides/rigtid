c this is <modulo.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 1972 by R. A. Broucke (JPL)
c
c modulo function (utility subroutine)
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c ============================================================================
c
      SUBROUTINE modulo(A,B)                                            
C MODULO SPECIAL PURPOSE SUBROUTINE IN BROUCKE EPHEMERIS SYSTEM         
C     A AND B ARE INPUT (B POSITIVE)                                    
C     A IS OUTPUT (BETWEEN 0 AND B)                                     
C     B IS NOT CHANGED BY THE SUBROUTINE                                
      DOUBLE PRECISION A,B,C                                            
      N=IDINT(A/B)                                                      
      C=DBLE(FLOAT(N))                                                  
      A=A-C*B                                                           
      IF(A.LT.0.0D0) A=A+B                                              
      RETURN                                                            
      END                                                               

c
c ----- END OF modulo.f ----- 
