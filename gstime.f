c this is <gstime.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 1972 by R. A. Broucke (JPL)
c
c compute greenwich siderial time from universal time
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c ============================================================================
c
      SUBROUTINE gstime(UT,AGST)                                        
C  COMPUTES GREENWICH SIDEREAL TIME,WHEN UNIVERSAL TIME IS GIVEN        
C  INPUT=UT=UNIVERSAL TIME IN JULIAN DAYS                               
C  OUTPUT=MGST=MEAN GREENWICH SIDEREAL TIME,IN HOURS                    
C         AGST=APPARENT GREENWICH SIDEREAL TIME,IN HOURS                
C  AGST=HOUR ANGLE OF VERNAL EQUINOX + 12.0 HOURS                       
C         EQEQ=EQUATION OF EQUINOXES,IN SECONDS                         
C         OBL =TRUE OBLIQUITY OF THE ECLIPTIC ,IN DEGREES               
      DOUBLE PRECISION UT,MGST,AGST,EQEQ,    RU,T,T2,A1,B1,XL,XO,MOBL,T3
     1,OBL,RAD                                                          
      DATA RAD/.0174532925199433D0/                                     
      A1=(UT-2415020.0D0)                                               
      T=A1/36525.0D0                                                    
      T2=T*T                                                            
      T3=T2*T                                                           
      RU=18.0D0+(2325.836D0+8640184.542D0*T+0.0929D0*T2)/3600.0D0       
      I1=IDINT(A1)                                                      
      B1=DBLE(FLOAT(I1))                                                
      MGST=(A1-B1)*24.0D0+12.0D0+RU                                     
      MGST=DMOD(MGST,24.0D0)                                            
      CALL NUTAN(UT,XL,XO)                                              
      MOBL=23.D0+(1628.26D0-46.845D0*T-.0059D0*T2+.00181D0*T3)/3600.D0  
      OBL=MOBL+XO/RAD                                                   
      EQEQ=XL*DCOS(RAD*OBL)/(RAD*15.0D0)                                
      AGST=MGST+EQEQ                                                    
      AGST=DMOD(AGST,24.0D0)                                            
      RETURN                                                            
      END                                                               

c
c ----- END OF gstime.f ----- 
