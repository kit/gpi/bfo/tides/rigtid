c this is <etmutc.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 1972 by Walter Zuern (UCLA)
c
c compute ephemeris time from UTC
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c REVISIONS and CHANGES
c    16/04/2000   WZ: extended table with 30 new values
c
c ============================================================================
c
      SUBROUTINE ETMUTC(DJ,Y)                                           
C  INPUT = DJ = TIME IN JULIAN YEARS RECKONED FROM THE FUNDAMENTAL EPOCH     
C              (NOT LESS THAN  55.5 )                                   
C  OUTPUT = Y  = DELTA T = ET - UTC   IN SECONDS                            
C     UTC IS COORDINATED UNIVERSAL TIME,USED IN RADIO BROADCASTS
C  THE TABLE HAS TO BE EXTENDED,WHEN NEW DATA ARE AVAILABLE.CHANGE NTAB. 
C     SOURCE OF INFORMATION: JET PROPULSION LABORATORY (DR.R.A.BROUCKE) 
C  The table was extended on April 16, 2000 using the corresponding 
C  routine in the ETERNA package, 30 new values were added.
      DOUBLE PRECISION DJ,Y                                             
      DIMENSION  TX(83),TY(83)                                          
      DATA  TX/                                                         
     155.5    ,56.5     ,57.5     ,58.5     ,59.5     ,60.5     ,61.5  ,
     262.     ,62.5     ,63.      ,63.5     ,64.      ,64.5     ,65.   ,
     365.5    ,66.      ,66.5     ,67.      ,67.5     ,68.      ,68.25 ,
     468.5    ,68.75    ,69.      ,69.25    ,69.5     ,69.75    ,70.   ,
     570.25   ,70.5     ,70.75    ,71.      ,71.085   ,71.162   ,71.247,
     671.329  ,71.414   ,71.496   ,71.581   ,71.666   ,71.748   ,71.833,
     771.915  ,71.999   ,72.0     ,72.499   ,72.5     ,72.9999  ,73.0  ,
     873.9999 ,74.0     ,74.9999  ,75.0     ,75.9999  ,76.0    ,76.9999,
     977.0    ,77.9999  ,78.0     ,78.9999  ,79.0     ,79.9999 ,80.0   ,
     181.4999 ,81.5     ,82.4999  ,82.5000  ,83.4999  ,83.500  ,85.4999,
     285.500  ,87.9999  ,88.0     ,89.9999  ,90.000   ,90.9999 ,91.0   ,
     392.4999 ,92.500   ,93.4999  ,93.50    ,94.4999  ,94.50   /
      DATA  TY /                                                        
     131.59   ,32.06    ,31.82    ,32.69    ,33.05    ,33.16    ,33.59 ,
     234.032  ,34.235   ,34.441   ,34.644   ,34.95    ,35.286   ,35.725,
     336.16   ,36.498   ,36.968   ,37.444   ,37.913   ,38.39    ,38.526,
     438.76   ,39.      ,39.238   ,39.472   ,39.707   ,39.946   ,40.185,
     540.42   ,40.654   ,40.892   ,41.131   ,41.211   ,41.284   ,41.364,
     641.442  ,41.522   ,41.600   ,41.680   ,41.761   ,41.838   ,41.919,
     741.996  ,42.184   ,42.184   ,42.184   ,43.184   ,43.184   ,44.184,
     844.184  ,45.184   ,45.184   ,46.184   ,46.184   ,47.184   ,47.184,
     948.184  ,48.184   ,49.184   ,49.184   ,50.184   ,50.184   ,51.184,
     151.184  ,52.184   ,52.184   ,53.184   ,53.184   ,54.184   ,54.184,
     255.184  ,55.184   ,56.184   ,56.184   ,57.184   ,57.184   ,58.184,
     358.184  ,59.184   ,59.184   ,60.184   ,60.184   ,61.184   /
      NTAB=83                                                           
      IF(DJ-TX(NTAB))  11,12,12                                         
   12 Y=TY(NTAB)                                                        
      RETURN                                                            
   11 DO  20  I=1,NTAB                                                  
      IF(DJ-TX(I))   21,22,20                                           
   22 Y=TY(I)                                                           
      RETURN                                                            
   21 N=I-1                                                             
      GO  TO  23                                                        
   20 CONTINUE                                                          
   23 Y=(TY(N+1)*(DJ-TX(N))-TY(N)*(DJ-TX(N+1)))/(TX(N+1)-TX(N))
      RETURN                                                            
      END                                                               
c
c ----- END OF etmutc.f ----- 
