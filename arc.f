c this is <arc.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 1972 by R. A. Broucke (JPL)
c
c compute angle from sin and cos
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c ============================================================================
c
      SUBROUTINE arc(COSA,SINA,ANGLE)                                  
      DOUBLE PRECISION COSA,SINA,ANGLE                                  
      IF(COSA)310,311,320                                               
  311 IF(SINA) 312,313,313                                              
  312 ANGLE=4.712388980384690D0                                         
      GO TO 350                                                         
  313 ANGLE=1.570796326794897D0                                         
      GO TO 350                                                         
  310 ANGLE=DATAN(SINA/COSA)+3.141592653589793D0                        
      GO TO 350                                                         
  320 ANGLE=DATAN(SINA/COSA)                                            
      IF(SINA) 315,350,350                                             
  315 ANGLE=ANGLE+6.283185307179586D0                                   
  350 RETURN                                                            
      END                                                              

c
c ----- END OF arc.f ----- 
