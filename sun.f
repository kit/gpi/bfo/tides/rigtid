c this is <sun.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 1972 by R. A. Broucke, P. Muller (JPL)
c
c compute sun earth ephemeris
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c ============================================================================
c
      SUBROUTINE sun(DJ,EL,C)                                           
C     SUN EARTH EPHEMERIS  R. BROUCKE CONVERTED TO 360                  
C     P. MULLER JPL  DEC 69                                             
C                                                                       
C     ROGER BROUCKE,  JET PROPULSION LABORATORY                         
C                                                                       
C     NEWCOMB EARTH EPHEMERIS, ASTRONOMICAL PAPERS, VOLUME 6, PART 1.   
C     THE FUNDAMENTAL EPOCH IS 1900,JAN 0,GREENWICH MEAN NOON=2415020.0 
C     THE TIME IS IN JULIAN DAYS (36525 DAYS PER JULIAN CENTURY)        
C     EL(1)=MEAN SEMI-MAJOR-AXIS OF THE EARTH, IN ASTRONOMICAL UNITS    
C     EL(2)=ECCENTRICITY OF THE EARTH'S ORBIT                           
C     EL(3)=MEAN OBLIQUITY OF THE EARTH, IN DEGREES                     
C     EL(4) AND EL(6) ARE NOT USED, (=ALWAYS ZERO)                      
C     EL(5)=LONGITUDE OF PERIGEE OF THE SUN, FREE FROM ABERRATION       
C     EL(7)=TIME IN JULIAN DAYS                                         
C     EL(8)=MEAN SIDEREAL MOTION OF THE SUN IN DEGREES PER JULIAN DAY   
C     EL(9)=MEAN GEOMETRIC LONGITUDE OF THE SUN, FREE FROM ABERRATION   
C     EL(10)=MEAN ANOMALY OF THE EARTH, IN DEGREES                      
C     EL(11)=MEAN RADIUS VECTOR OF THE EARTH, IN ASTRONOMICAL UNITS     
C     EL(12)=EQUATION OF THE CENTER OF THE EARTH, IN DEGREES            
C        = (TRUE-MEAN) ANOMALY                                          
C     EL(13)=RIGHT ASCENSION OF FICTITIOUS MEAN SUN, FREE FROM          
C        ABERRATION, IN HOURS =                                         
C        =MEAN LONGITUDE OF FICTITIOUS MEAN SUN                         
C        =GREENWICH SIDEREAL TIME OF MEAN NOON, UNCORRECTED FOR NUTATION
C        =SIDEREAL TIME MINUS MEAN GREENWICH TIME(UNCORRECTED FOR NUTAT)
C     SIDEREAL TIME=MEAN R.A.M.S. +NUTATION IN R.A.                     
C     THE NUTATION IN R.A. IS(NUT.IN LONGITUDE)*COSINE(TRUE OBLIQUITY), 
C        DIVIDED BY 15 TO REDUCE TO SECONDS OF TIME                     
C     EL(14)=MEAN LONGITUDE OF THE MOON, IN DEGREES                     
C     EL(15)=MEAN ANOMALY OF THE MOON, IN DEGREES                       
C        EL(15)=EL(14)-EL(21)                                           
C     EL(16)=MEAN ARGUMENT OF LATITUDE OF THE MOON, IN DEGREES          
C     EL(17)=MEAN LONGITUDE OF THE ASCENDING NODE OF THE MOON           
C     EL(18)=PRECESSIONAL CONSTANT IN DEGREES PER JULIAN CENTURY        
C     EL(19)=NUTATION IN LONGITUDE IN DEGREES                           
C     EL(20)=NUTATION IN OBLIQUITY IN DEGREES                           
C     EL(21)=MEAN LONGITUDE OF PERIGEE OF THE MOON IN DEGREES           
C     C(1)=TRUE RADIUS VECTOR OF THE SUN, IN ASTRONOMICAL UNITS         
C     C(2)=SUN'S TRUE GEOMETRIC LONGITUDE,W.R.TO MEAN EQUINOX OF DATE   
C     C(2) IS IN DEGREES AND C(4)=C(2), BUT IN RADIANS                  
C     TO HAVE THE APPARENT LONGITUDE OF THE SUN, THE ABERRATION MUST    
C     BE SUBTRACTED                                                     
C     TO HAVE THE LONGITUDE W.R.TO THE TRUE EQUINOX, ADD NUTAT IN LONG. 
C     C(3)=SUN'S TRUE GEOMETRIC LATITUDE                                
C     C(3) IS IN DEGREES AND C(5)=C(3), BUT IN RADIANS                  
C     REMARK. THE NUTATION HAS NO EFFECT ON THE LATITUDE.               
      external modulo
      DOUBLE PRECISION DJ,EL,C,T,TP,GME,GVE,GJU,GMA,GSA,GPL,T2,T3       
     1,DV,DR,DB,R0,RJ,RI,DEGRAD,RADDEG,TWOPI, GRAD, AUP, AD             
     2,E,SMALOG                                                         
      DOUBLE PRECISION REGRAD                                           
      REAL SL,SR,SLA,AL,AR,ALA,ARV,ARR,ARG                              
      INTEGER JLR,ILR,JLA,ILA                                           
      DIMENSION EL(21),C(5),JLR(120),ILR(120),JLA(34),ILA(34),E(11),    
     1SL(120),SR(120),SLA(34),AL(120),AR(120),ALA(34)                   
      DATA DEGRAD,RADDEG,TWOPI                                          
     1/.017453292519943296D0,57.295779513082321D0,6.2831853071795864D0/ 
C     THE COEFFICIENTS OF FOURIER SERIES ARE IN THE 10 FOLLOWING TABLES 
C     JLR=INDEX J FOR LONGITUDE AND RADIUS                              
      DATA  JLR   /-1,-1,-1,-1,-1,-1,-1,-1,-2,-2,-2,-2,-2,              
     1-3,-3,-3,-3,-3,-4,-4,-4,-4,-4,-5,-5,-5,-5,-6,-6,-6,-6,-7,-7,-7,-7,
     2-8,-8,-8,-8,-8,-9,-9,-10,+1,+1,+1,+2,+2,+2,+2,+3,+3,+3,+3,        
     3+4,+4,+4,+4,+5,+5,+5,+5,+6,+6,+6,+6,+7,+7,+7,+8,+8,+8,+8,+9,+9,+9,
     4+10,+10,+10,+11,+11,+12,+13,+13,+15,+15,+17,+17,+1,+1,+1,+1,+1,   
     5+2,+2,+2,+2,+3,+3,+3,+3,+4,+4,+4,+4,+5,+5,+5,+5,+1,+1,+1,+1,      
     6+2,+2,+2,+2,+3,+3,+4/                                             
C     ILR=INDEX I FOR LONGITUDE AND RADIUS                              
      DATA  ILR    /+1,+2,+3,+4,+0,+1,+2,+3,+0,+1,+2,+3,+4,             
     1+2,+3,+4,+5,+6,+3,+4,+5,+6,+7,+5,+6,+7,+8,+6,+7,+8,+9,+7,+8,+9,+10
     2,+8,+9,+12,+13,+14,+9,+10,+10,-2,-1,+0,-3,-2,-1,+0,-4,-3,-2,-1,   
     3-4,-3,-2,-1,-5,-4,-3,-2,-6,-5,-4,-3,-6,-5,-4,-7,-6,-5,-4,         
     4-7,-6,-5,-7,-6,-5,-7,-6,-7,-8,-7,-9,-8,-10,-9,-3,-2,-1,+0,+1,     
     5-3,-2,-1,+0,-4,-3,-2,-1,-4,-3,-2,-1,-5,-4,-3,-2,-2,-1,+0,+1,      
     6-3,-2,-1,+0,-2,-1,-2/                                             
C     JLA=INDEX J FOR LATITUDE                                          
      DATA  JLA    /-1,-1,-1,-1,-2,-2,-2,-2,-3,-3,-3,-3,-3,             
     1-4,-4,-4,-5,-5,-6,-6,-6,-8,+2,+2,+4,+1,+1,+1,+1,+2,+3,+3,+1,+1/   
C     ILA=INDEX I FOR LATITUDE                                          
      DATA  ILA     /+0,+1,+2,+3,+1,+2,+3,+4,+2,+3,+4,+5,+6,            
     1+3,+5,+6,+6,+7,+5,+7,+8,+12,-2,+0,-3,-2,-1,+0,+1,-1,-2,-1,-1,+1/  
C     SL = COEFFICIENTS S FOR LONGITUDE                                 
      DATA  SL   /.013,.005,.015,.023,.075,4.838,.074,.009,             
     1.003,.116,5.526,2.497,.044,.013,.666,1.559,1.024,.017,.003,.210,  
     2.144,.152,.006,.084,.037,.123,.154,.038,.014,.010,.014,.020,      
     3.006,.005,0.0,.011,0.0,.042,0.0,.023,.006,0.0,.003,.006,.273,.048,
     4.041,2.043,1.770,.028,.004,.129,.425,.008,.034,.500,.585,.009,    
     5.007,.085,.204,.003,0.0,.020,.154,.101,.006,.049,.106,.003,.010,  
     6.052,.021,.004,.028,.062,.005,.019,.005,.017,.044,.006,.013,.045, 
     7.021,0.0,.004,.026,.003,.163,7.208,2.600,.073,.069,2.731,1.610,   
     8.073,.005,.164,.556,.210,.016,.044,.080,.023,0.0,.005,.007,.009,  
     9.011,.419,.320,.008,0.0,.108,.112,.017,.021,.017,.003/            
C     SR = COEFFICIENTS S FOR RADIUS                                    
      DATA  SR   /28.,6.,18.,5.,94.,2359.,69.,16.,4.,160.,              
     16842.,869.,52.,21.,1045.,1497.,194.,19.,6.,376.,196.,94.,6.,      
     2163.,59.,141.,26.,80.,25.,14.,12.,42.,12.,4.,4.,24.,6.,44.,       
     312.,33.,13.,4.,8.,8.,150.,28.,52.,2057.,151.,31.,6.,168.,215.,    
     46.,49.,478.,105.,10.,12.,107.,89.,3.,5.,30.,139.,27.,10.,60.,     
     538.,5.,15.,45.,8.,6.,34.,17.,8.,15.,0.0,20.,9.,5.,15.,5.,22.,     
     66.,4.,0.0,5.,208.,7067.,244.,80.,103.,4026.,1459.,8.,9.,281.,803.,
     7174.,29.,74.,113.,17.,3.,10.,12.,14.,15.,429.,8.,8.,3.,162.,      
     8112.,0.0,32.,17.,4./                                              
C     SLA = COEFFICIENTS S FOR LATITUDE                                 
      DATA  SLA   /.029,.005,.092,.007,.023,.012,.067,.014,.014,        
     1.008,.210,.007,.004,.006,.031,.012,.009,.019,.006,.004,.004,.010, 
     2.008,.008,.007,.007,.017,.016,.023,.166,.006,.018,.006,.006/      
C     AL = PHASE ANGLE K FOR LONGITUDE                                  
      DATA AL    /243.,225.,357.,326.,296.6,299.101667,207.9,           
     1249.,162.,148.9,148.313333,315.943333,311.4,176.,177.71,345.25333,
     2318.15,315.,198.,206.2,195.4,343.8,322.,235.6,221.8,195.3,359.6,  
     3264.1,253.,230.,12.,294.,279.,288.,0.0,322.,0.0,259.2,0.0,48.8,   
     4351.,0.0,18.,218.,217.7,260.3,346.,343.888,200.402,148.,284.,     
     5294.2,338.88,7.,71.,105.18,334.06,325.,172.,54.6,100.8,18.,0.0,   
     6186.,227.4,96.3,301.,176.5,222.7,72.,307.,348.9,215.2,57.,298.,   
     7346.,68.,111.,338.,59.,105.9,232.,184.,227.8,309.,0.0,243.,113.,  
     8198.,198.6,179.532,263.217,276.3,80.8,87.145,109.493,252.6,158.,  
     9170.5,82.65,98.5,259.,168.2,77.7,93.,0.0,259.,164.,71.,105.,      
     1100.58,269.46,270.,0.0,290.6,293.6,277.,289.,291.,288./           
C     AR = PHASE ANGLE K FOR RADIUS                                     
      DATA  AR   /335.,130.,267.,239.,205.,209.080,348.5,               
     1330.0,90.,58.4,58.318,226.7,38.8,90.,87.570,255.25,49.5,43.,      
     290.,116.28,105.2,254.8,59.,145.4,132.2,105.4,270.,174.3,164.,     
     3135.,284.,203.5,194.,166.,135.,234.,218.,169.7,222.,138.7,261.,   
     4256.,293.,130.,127.7,347.,255.4,253.828,295.,234.3,180.,203.5,    
     5249.,90.,339.7,15.17,65.9,53.,90.,324.6,11.,108.,217.,95.7,137.3, 
     6188.,209.,86.2,132.9,349.,217.,259.7,310.,329.,208.1,257.,337.,   
     723.,0.0,30.,21.,143.,94.,143.,220.,261.,153.,0.0,112.,112.,       
     889.545,338.6,6.5,350.5,357.108,19.466,263.000,69.,81.2,352.56,    
     98.6,170.,79.9,347.7,3.,252.,169.,76.,343.,11.,10.6,353.,0.0,      
     1198.,200.6,203.1,0.0,200.1,201.,194./                             
C     ALA = PHASE ANGLE FOR LATITUDE                                    
      DATA  ALA   /145.,323.,93.7,262.,173.,149.,123.,111.,201.,        
     1187.,151.8,153.,296.,232.,1.8,180.,27.,18.,288.,57.,57.,61.,90.,  
     2346.,188.,180.,273.,180.,268.,265.5,171.,267.,260.,280./          
C     T = TIME IN CENTURIES FROM 1900.0 (2415020.0)                     
C     TP = TIME IN YEARS FROM 1850.0                                    
      REGRAD = DEGRAD                                                   
      T=(DJ-2415020.0D0)/36525.D0                                       
      TP=(DJ-2396758.203D0)/365.25D0                                    
      T2=T*T                                                            
      T3=T2*T                                                           
      S=T                                                               
C     COMPUTE POLYNOMIALS FOR MEAN ELEMENTS                             
      EL(1)=1.00000023D0                                                
      EL(2)=.01675104D0-.00004180D0*T-.000000126D0*T2                   
      EL(3)=23.D0+(1628.26D0-46.845D0*T-.0059D0*T2+.00181D0*T3)/3600.D0 
      EL(4)=0.0D0                                                       
      EL(5)=281.D0+(795.0D0+6189.03D0*T+1.63D0*T2+.012D0*T3)/3600.D0    
      EL(6)=0.0D0                                                       
      EL(7)=DJ                                                          
      EL(8)=(1295977.432D0-.000403D0*T)/(3600.D0*365.25D0)              
      EL(9)=279.D0+(2508.04D0+129602768.13D0*T+1.089D0*T2)/3600.D0      
      EL(10)=EL(9)-EL(5)                                                
      EL(13)=18.D0+(2325.836D0+8640184.542D0*T+.0929D0*T2)/3600.D0      
      CALL modulo(EL(10),360.D0)                                        
      GRAD=EL(10)*DEGRAD                                                
      E(1)=EL(2)                                                        
      E(2)=E(1)*E(1)                                                    
      E(3)=E(2)*E(1)                                                    
      E(4)=E(3)*E(1)                                                    
      E(5)=E(4)*E(1)                                                    
      E(6)=E(5)*E(1)                                                    
      E(7)=E(6)*E(1)                                                    
      E(8)=E(7)*E(1)                                                    
C     COMPUTE EQUATION OF CENTER ,EL(12)                                
      EL(12)=RADDEG*(                                                   
     1+DSIN(1.0D0*GRAD)*(2.D0*E(1)-.25D0*E(3)+5.D0/96.D0*E(5)           
     1                                       +107.D0/4608.D0*E(7))      
     2+DSIN(2.0D0*GRAD)*(1.25D0*E(2)-11.D0/24.D0*E(4)+17.D0/192.D0*E(6) 
     2                                       +43.D0/5760.D0*E(8))       
     3+DSIN(3.0D0*GRAD)*(13.D0/12.D0*E(3)-43.D0/64.D0*E(5)              
     3                                       +95.D0/512.D0*E(7))        
     4+DSIN(4.0D0*GRAD)*(103.D0/96.D0*E(4)-451.D0/480.D0*E(6)           
     4                                       +4123.D0/11520.D0*E(8))    
     5+DSIN(5.0D0*GRAD)*(1097.D0/960.D0*E(5)-5957.D0/4608.D0*E(7))      
     6+DSIN(6.0D0*GRAD)*(1223.D0/960.D0*E(6)-7913.D0/4480.D0*E(8))      
     7+DSIN(7.0D0*GRAD)*(47273.D0/32256.D0*E(7)))                       
C     COMPUTE KEPLERIAN PART OF LOG(R) ,R0                              
      SMALOG=+0.00000010D0                                              
      R0=     SMALOG+0.43429448190325183D0*                             
     1                (+(.25D0*E(2)+1.D0/32.D0*E(4)+1.D0/96.D0*E(6)     
     1                                     +5.D0/1024.D0*E(8))          
     1+DCOS(1.0D0*GRAD)*(-E(1)+.375D0*E(3)+1.D0/64.D0*E(5)              
     1                                     +127.D0/9216.D0*E(7))        
     2+DCOS(2.0D0*GRAD)*(-.75D0*E(2)+11.D0/24.D0*E(4)-3.D0/64.D0*E(6)   
     2                                       +9.D0/640.D0*E(8))         
     3+DCOS(3.0D0*GRAD)*(-17.D0/24.D0*E(3)+77.D0/128.D0*E(5)            
     3                                       -743.D0/5120.D0*E(7))      
     4+DCOS(4.0D0*GRAD)*(-71.D0/96.D0*E(4)+129.D0/160.D0*E(6)           
     4                                       -387.D0/1280.D0*E(8))      
     5+DCOS(5.0D0*GRAD)*(-523.D0/640.D0*E(5)+10039.D0/9216.D0*E(7))     
     6+DCOS(6.0D0*GRAD)*(-899.D0/960.D0*E(6)+6617.D0/4480.D0*E(8))      
     7+DCOS(7.0D0*GRAD)*(-355081.D0/322560.D0*E(7)))                    
      EL(11)=10.0D0**R0                                                 
      CALL modulo(EL(5),360.D0)                                         
      CALL modulo(EL(9),360.D0)                                         
      CALL modulo(EL(13),24.D0)                                         
C     COMPUTE MEAN ANOMALIES OF PERTURBING PLANETS                      
      GME=248.070D0+1494.72350D0*TP                                     
      GVE=114.500D0+ 585.17493D0*TP                                     
      GMA=109.856D0+ 191.39977D0*TP                                     
      GJU=148.031D0+  30.34583D0*TP                                     
      GSA=284.716D0+  12.21794D0*TP                                     
C     COMPUTE PERIODIC PERTURBATIONS FOR LONGITUDE AND LOG(R)           
      DV=0.0D0                                                          
      DR=0.0D0                                                          
      DO 100 I=1,120                                                    
      IF(I.LE.4) GPL=GME                                                
      IF(I.GE.5.AND.I.LE.43) GPL=GVE                                    
      IF(I.GE.44.AND.I.LE.88) GPL=GMA                                   
      IF(I.GE.89.AND.I.LE.109) GPL=GJU                                  
      IF(I.GE.110) GPL=GSA                                              
      RJ=JLR(I)                                                         
      RI=ILR(I)                                                         
      ARG=RJ*GPL+RI*EL(10)                                              
      ARV=(ARG-AL(I))*DEGRAD                                            
      ARR=(ARG-AR(I))*DEGRAD                                            
      DV=DV+DBLE(SL(I)*COS(ARV))                                        
 100  DR=DR+DBLE(SR(I)*COS(ARR))                                        
C     COMPUTE PERIODIC PERTURBATIONS FOR LATITUDE                       
      DB=0.0D0                                                          
      DO 200 I=1,34                                                     
      IF(I.LE.22) GPL=GVE                                               
      IF(I.GE.23.AND.I.LE.25) GPL=GMA                                   
      IF(I.GE.26.AND.I.LE.32) GPL=GJU                                   
      IF(I.GE.33) GPL=GSA                                               
      RJ=JLA(I)                                                         
      RI=ILA(I)                                                         
      ARG=(RJ*GPL+RI*EL(10)-ALA(I))*DEGRAD                              
 200  DB=DB+DBLE(SLA(I)*COS(ARG))                                       
      DV=DV+.266D0*SIN((31.8+119.0*S)*REGRAD)                           
     1     +6.40D0*SIN((231.19+20.20*S)*REGRAD)                         
     2+(1.882-.016*T)*SIN((57.24+150.27*S)*REGRAD)                      
     3+0.20*DCOS((15.*GMA-8.*EL(10))*DEGRAD)                            
     4-0.03*DSIN((15.*GMA-8.*EL(10))*DEGRAD)                            
      EL(21)=334.D0+109.D0*T+(1186.40D0+122.52D0*T-37.17D0*T2-.045D0*T3)
     1/3600.D0+11.D0*TWOPI*RADDEG*T                                     
      CALL modulo(EL(21),360.D0)                                        
      EL(14)=270.D0+307.D0*T+(1562.99D0+3179.31D0*T-4.08D0*T2+.68D-2*T3)
     1/3600.D0+ 1336.D0*TWOPI*RADDEG*T                                  
      CALL modulo(EL(14),360.D0)                                        
      EL(15)=296.D0+198.D0*T                                            
     1      +(376.59D0+3056.79D0*T+33.09D0*T2+.0518D0*T3)/3600.D0       
     2     +(1325.D0*T*TWOPI*RADDEG)                                    
      CALL modulo(EL(15),360.D0)                                        
      EL(17)=259.D0-134.D0*T+(659.79D0-511.23D0*T+7.48D0*T2+.008D0*T3)  
     1/3600.D0-5.D0*TWOPI*RADDEG*T                                      
      CALL modulo(EL(17),360.D0)                                        
      EL(16)=EL(14)-EL(17)                                              
      CALL modulo(EL(16),360.D0)                                        
      EL(18)=(5489.90D0-0.00364D0*T)/3600.D0                            
      AD=EL(14)-EL(9)                                                   
      AUP=EL(9)-EL(17)                                                  
      DV=DV+6.454*DSIN(AD*DEGRAD)                                       
     1     +0.013*DSIN(3.*AD*DEGRAD)                                    
     2     +0.177*DSIN((AD+EL(15))*DEGRAD)                              
     3     -0.424*DSIN((AD-EL(15))*DEGRAD)                              
     4     +0.039*DSIN((3.*AD-EL(15))*DEGRAD)                           
     5     -0.064*DSIN((AD+EL(10))*DEGRAD)                              
     6     +0.172*DSIN((AD-EL(10))*DEGRAD)                              
     7     -0.013*DSIN((AD-EL(15)-EL(10))*DEGRAD)                       
     8     -0.013*DSIN(2.*AUP*DEGRAD)                                   
      DR=(+1336.*DCOS(DEGRAD*(AD))                                      
     1     +0003.*DCOS(DEGRAD*(3.*AD))                                  
     2     +0037.*DCOS(DEGRAD*(AD+EL(15)))                              
     3     -0133.*DCOS(DEGRAD*(AD-EL(15)))                              
     4     +0008.*DCOS(DEGRAD*(3.*AD-EL(15)))                           
     5     -0014.*DCOS(DEGRAD*(AD+EL(10)))                              
     6     +0036.*DCOS(DEGRAD*(AD-EL(10)))                              
     7     -0003.*DCOS(DEGRAD*(AD-EL(15)-EL(10)))                       
     8     +0003.*DCOS(DEGRAD*(2.*AUP)))*10.0D0+DR                     
      DB=  +.576*DSIN(DEGRAD*(EL(16)))                                  
     1     +.016*DSIN(DEGRAD*(EL(16)+EL(15)))                           
     2     -.047*DSIN(DEGRAD*(EL(16)-EL(15)))                           
     3     +.021*DSIN(DEGRAD*(EL(16)-2.*AUP))                           
     4     +.005*DSIN(DEGRAD*(EL(16)-2.*AUP-EL(15)))                    
     5     +.005*DSIN(DEGRAD*(EL(16)+EL(10)))                           
     6     +.005*DSIN(DEGRAD*(EL(16)-EL(10)))-DB                        
C     COMPUTE THE COORDINATES                                           
 600  C(1)=10.D0**(R0+DR*1.D-9)                                         
      C(2)=EL(9)+DV/3600.D0+EL(12)                                      
      CALL modulo(C(2),360.D0)                                          
      C(4)=C(2)*DEGRAD                                                  
      C(3)=DB/3600.D0                                                   
      C(5)=C(3)*DEGRAD                                                  
      RETURN                                                            
      END                                                               

c
c ----- END OF sun.f ----- 
