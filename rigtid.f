c this is <rigtid.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 1972 by Walter Zuern (UCLA)
c
c compute tidal acceleration of sun and moon
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c REVISIONS and CHANGES
c    spring 2000     WZ: checked for Y2K problem through comparison with
c                        ETGTAB3
c
c ============================================================================
c
      SUBROUTINE RIGTID(HT,AL,AM,OL,OM)                                 
C  THIS SUBROUTINE AND THE ONES CALLED COMPUTE VERTICAL (POSITIVE UPWARD
C  HORIZONTAL TIDAL ACCELERATIONS DUE TO SUN AND MOON.THE BASIC FEATURES
C   THE PROGRAM AND ITS ACCURACY ARE DISCUSSED IN                       
C      BROUCKE,R.A.;ZURN,W;SLICHTER,L.B.                                
C      LUNAR TIDAL ACCELERATION ON A RIGID EARTH                        
C      GEOPHYSICAL MONOGRAPH 16,FLOW AND FRACTURE OF ROCKS              
C      (THE GRIGGS VOLUME),A.G.U.,WASH.,1972,319-324                    
      IMPLICIT REAL*8(A-H,O-Z)                                          
      DOUBLE PRECISION XELSUN(21),XCSUN(5)                              
C                                                                       
C  INPUT PARAMETERS                                                     
C                                                                       
C     HT   STATION HEIGHT ABOVE SEA LEVEL IN METERS                     
C     OL   DEGREES OF STATION LONGITUDE , WEST + ,EAST -                
C     OM   MINUTES OF STATION LONGITUDE                                 
C     AL   DEGREES OF STATION LATITUDE, SOUTH -                         
C     AL   MINUTES OF STATION LATITUDE                                  
c  This is the version tested in spring of 2000, during studies of the 
c  South Pole Mf & Mm tides for Leon's 75th birthday and in this context
c  after the complaint by the Open University (thru Trevor Baker) about a
c  Y2K-Problem. So far I could not find that problem by comparison with 
c  ETGTAB3 it has the usual 10 nanogals or less accuracy.   
      REAL*8  CNZERO  /0.0D0/                                           
      REAL*8  CNHALF /0.5D0/                                            
      REAL*8  CN1 /1.0D0/                                               
      REAL*8  CN2 /2.0D0/                                               
      REAL*8  CN3  /3.0D0/                                              
      REAL*8  CN4 /4.0D0/                                               
      REAL*8  CN5  /5.0D0/                                              
      REAL*8  CN12 / 12.0D0/                                            
      REAL*8  CN15 /15.0D0/                                             
      REAL*8  CN24 /24.0D0/                                             
      REAL*8  CN35  /35.0D0/                                            
      REAL*8  CN60 /60.0D0/                                             
      REAL*8  CN100  /100.0D0/                                          
      REAL*8  CN360 /360.0D0/                                           
      REAL*8  CN365 /365.0D0/                                           
      REAL*8  C36525  /36525.0D0/                                       
      REAL*8  W  /23.4519591D0/                                         
C     SCALED 10**(-3)                                                   
      REAL*8  RADIUS  /6.37816D0/                                       
C     SCALED 10**(-3)                                                   
      REAL*8  CVTKM   /.000001D0/                                       
C     SCALED 10**(-8)                                                   
      REAL*8   ESDIST /1.495985D0/                                      
      REAL*8 ASTRUN/1.49597870D0/                                       
C     SCALED   10**(-19)                                                
      REAL*8  GTME   /39.8603D0/                                        
C     SCALED   10**(-2)                                                 
      REAL*8   RMEM   / 0.8130D0/                                       
C     SCALED   10**(-6)                                                 
      REAL*8   RMSE   /0.332958D0/                                      
      REAL*8 SCLGM1 /10000000.0D0/                                      
      REAL*8  R1 /.00673966D0/                                          
C                                                                       
      REAL*8  DEGRAD /  .017453292519943D0/                             
      REAL*8  EPOCH /2415020.0D0/                                       
      REAL*8  TWOPI /  6.283185307179586D0/                             
      REAL*8  RNDOFF /.00001D0/                                         
C                                                                       
      REAL*8  H0  /279.69668D0/                                         
      REAL*8  H1  /36000.76892D0/                                       
      REAL*8  H2  /0.0003D0/                                            
C                                                                       
      REAL*8  ES0  /0.01675104D0/                                       
      REAL*8  ES1  /0.0000418D0/                                        
      REAL*8  ES2  /0.000000126D0/                                      
C                                                                       
      REAL*8  PS0  /281.22083D0/                                        
      REAL*8  PS1  /1.71902D0/                                          
      REAL*8  PS2  /0.00045D0/                                          
      REAL*8  PS3  /0.000003D0/                                         
C                                                                       
C  COMPUTE PROGRAM CONSTANTS                                            
C                                                                       
      SINW = DSIN(W*DEGRAD)                                             
      COSW = DCOS(W*DEGRAD)                                             
C                                                                       
      CF1 = (CN1 + COSW)*CNHALF                                         
      CF2 = (CN1 - COSW)*CNHALF                                         
C                                                                       
      ESDIST=ASTRUN                                                     
      DS0 = CN1/ESDIST                                                  
C                                                                       
      GM0=GTME*SCLGM1/RMEM                                              
      GM1=CN3*GTME*SCLGM1/(RMEM*CN2)                                    
C                                                                       
      GS0=GTME*RMSE                                                     
C                                                                       
      R0 = CN1 + R1                                                     
C                                                                       
      HT=HT*CVTKM                                                       
      ALM=(AL+AM/CN60)*DEGRAD                                           
C    COMPUTE GEOCENTRIC LATITUDE                                        
      GL=ALM                                                            
      POLES=CN4*DABS(ALM)                                               
      IF(POLES.EQ.TWOPI)    GO TO 3                                     
      ALM=DATAN(DSIN(ALM)/DCOS(ALM)/R0)                                 
 3    CONTINUE                                                          
      COSAL = DCOS(ALM)                                                 
      SINAL = DSIN(ALM)                                                 
      R = HT + RADIUS/DSQRT(R0 - R1*COSAL**2)                           
      OLM=OL
      OMM=OM
      RETURN                                                            
      ENTRY RIGTIM(YRS,DAYSR,HRSD,GS,GM,DAYSD,T,TUJD,TEJD,HEM,HSM,HES,HS
     1S)                                                               
C                                                                       
C  INPUT PARAMETERS                                                     
C                                                                       
C     YRS  LAST TWO DIGITS OF YEAR IN 20TH CENTURY, i. e. the year 2000 must
C          be given by 100                      
C     DAYSR    DAY OF THE YEAR                                          
C     HRSD    HOURS AND FRACTION OD HOURS IN GREENWICH MEAN TIME        
C   OUTPUT PARAMETERS                                                   
C     GS   TIDAL ACCELERATION BY THE SUN                                
C     GM   TIDAL ACCELERATION BY THE MOON                               
C   T IS THE NUMBER OF JULIAN CENTURIES SINCE DEC.31,1899               
C    DAYSD IS THE NUMBER OF DAYS PAST SINCE NOON DEC. 31,1899 ,         
C    THE IDINT EXPRESSION IS THE NUMBER OF LEAP YEARS PAST(NOTE THAT     
C    ALL YEARS DIVISIBLE BY 4 EXCEPT 1900 ARE LEAP YEARS). 2000 is also
C    a leap year, therefore the idint expression works in the 21. century.   
C     TUJD JULIAN DATE IN DAYS AND FRACTIONS OF DAYS (UNIVERSAL TIME)   
C     TEJD JULIAN DATE IN DAYS AND FRACTIONS OF DAYS ( EPHEMERIS TIME)  
C   HRSD IS GREENWICH MEAN TIME                                         
C     TJY  JULIAN DATE IN YEARS AND FRACTION OF YEARS (UNIVERSAL)       
C      HORIZONTAL TIDAL ACCELERATIONS IN MICROGAL                       
C                                                                       
C        HSM - MOON,SOUTH                                               
C        HEM - MOON,EAST                                                
C        HSS - SUN,SOUTH                                                
C        HES - SUN,EAST                                                 
C                                                                       
C                                                                       
C                                                                       
C                                                                       
      DAYSD = YRS*CN365 + DAYSR - CNHALF + IDINT((YRS - CN1)/CN4        
     1  + RNDOFF) + HRSD/CN24                                           
C                                                                       
      T = DAYSD/C36525                                                  
C                                                                       
      TUJD=DAYSD+EPOCH                                                  
      TJY=DAYSD/C36525*CN100                                            
C                                                                       
      CALL ETMUTC(TJY,ETUT)                                             
      TEJD=TUJD+ETUT/CN24/CN60/CN60                                     
C                                                                       
      CALL GSTIME(TUJD,AGST)                                            
      AGST=(AGST-CN12)*CN15*DEGRAD                                      
C                                                                       
C                                                                       
C    POSITION AND VERTICAL TIDAL ACCELERATION,SUN                       
      CALL SUN(TEJD,XELSUN,XCSUN)                                       
      W=XELSUN(3)                                                       
      XCSUN(4)=XCSUN(4)-20.47D0/CN60/CN60/XCSUN(1)*DEGRAD               
      DS=DS0/XCSUN(1)                                                   
      CALL NUTAN(TEJD,ANUTL,ANUTO)                                      
      DSOLS=DSIN(XCSUN(4)+ANUTL)                                        
      DSOLC=DCOS(XCSUN(4)+ANUTL)                                        
      DSALS=DSIN(XCSUN(5))                                              
      DSALC=DCOS(XCSUN(5))                                              
      OBLIQ=W*DEGRAD+ANUTO                                              
      SINOB=DSIN(OBLIQ)                                                 
      COSOB=DCOS(OBLIQ)                                                 
      SINDS=SINOB*DSOLS*DSALC+COSOB*DSALS                               
      COSDS=DSQRT(CN1-SINDS*SINDS)                                      
      COSHS=DSOLC*DSALC/COSDS                                           
      SINHS=(COSOB*DSOLS*DSALC-SINOB*DSALS)/COSDS                       
      HA=AGST-(OLM+OMM/CN60)*DEGRAD                                     
      COSHA=DCOS(HA)                                                    
      SINHA=DSIN(HA)                                                    
      TAUCOS=COSHA*COSHS+SINHA*SINHS                                    
      TAUSIN=SINHA*COSHS-COSHA*SINHS                                    
      CF=SINAL*SINDS+COSAL*COSDS*TAUCOS                                 
C                                                                       
C                                                                       
      GSK0=GS0*R*DS**3                                                  
      GS=GSK0*(CN3*CF**2-CN1)                                           
C                                                                       
C    POSITION AND VERTICAL TIDAL AVCELERATION,MO6N                      
C                                                                       
      CALL CRMOON(TEJD,DEKLIN,RIGAS,PAR)                                
      DA=DSIN(PAR)/RADIUS                                               
      TAU =AGST-RIGAS-(OLM+OMM/CN60)*DEGRAD                             
      TAU=DMOD(TAU,TWOPI)                                               
      COSTAU=DCOS(TAU)                                                  
      SINTAU=DSIN(TAU)                                                  
      SINDEK=DSIN(DEKLIN)                                               
      COSDEK=DCOS(DEKLIN)                                               
      CT=SINAL*SINDEK+COSAL*COSDEK*COSTAU                               
C                                                                       
      GMK0=GM0*R*DA**3                                                  
      GMK1=GM1*(R**2)*(DA**4)                                           
      GMK2=GM0*(R**3)*(DA**5)                                           
C                                                                       
      GM=GMK0*(CN3*CT**2-CN1)+GMK1*(CN5*CT**3-CN3*CT)                   
      GM=GM+GMK2*(CN3/CN2+CN35/CN2*CT**4-CN15*CT**2)                    
C                                                                       
C  HORIZONTAL ACCELERATIONS DUE TO THE SUN                              
C                                                                       
      HAID=GSK0*CN3*CF                                                  
      HES= -HAID*COSDS*TAUSIN                                           
      HSS=HAID*(COSAL*SINDS-SINAL*COSDS*TAUCOS)                         
      HSS=-HSS                                                          
C                                                                       
C     HORIZONTAL ACCELERATIONS DUE TO THE MOON                          
      HAID=GMK0*CN3*CT+GMK1*(CN5*CT**2-CN1)                             
      HAID=HAID+GMK2*(CN35/CN2*CT**3-CN15/CN2*CT)                       
      HEM=-HAID*COSDEK*SINTAU                                           
      HSM=HAID*(COSAL*SINDEK-COSTAU*COSDEK*SINAL)                       
      HSM=-HSM                                                          
C    TRANSFORM TO THE NORMAL ON THE SPHEROID                            
      COSPSI=COS(GL-ALM)                                                
      SINPSI=SIN(GL-ALM)                                                
      GSC=GS*COSPSI-HSS*SINPSI                                          
      HSS=HSS*COSPSI+GS*SINPSI                                          
      GS=GSC                                                            
      GMC=GM*COSPSI-HSM*SINPSI                                          
      HSM=HSM*COSPSI+GM*SINPSI                                          
      GM=GMC  
c      print *, GM                                                          
      RETURN                                                            
      END                                                              

 
c
c ----- END OF rigtid.f ----- 
