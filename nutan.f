c this is <nutan.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 1972 by R. A. Broucke (JPL)
c
c compute nutation in longitude and obliquity
c
c ----
c This program is free software: you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation, either version 3 of the License, or
c (at your option) any later version.
c
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c
c You should have received a copy of the GNU General Public License
c along with this program.  If not, see <https://www.gnu.org/licenses/>.
c ----
c
c ============================================================================
c
      SUBROUTINE nutan(XJD,XNUTL,XNUTO)                                 
C     COMPUTES NUTATION IN LONGITUDE AND OBLIQUITY                      
C  INPUT=XJD= TIME IN JULIAN DAYS                                       
C  OUTPUT=XNUTL=NUTATION IN LONGITUDE (IN  RADIANS)                     
C     AND XNUTO=NUTATION IN OBLIQUITY (IN  RADIANS)                     
      DOUBLE PRECISION  XJD,PL,PO,XNUTL,XNUTO                           
      DOUBLE PRECISION A(5,4),FA(5)                                     
      DOUBLE PRECISION T                                                
      REAL XL(13),XLP(13),XFX(13),XD(13),XN(13),SLC(13),SLT(13),COC(13),
     *COT(13)                                                           
      REAL SA(5)                                                        
      DOUBLE PRECISION TWOPI                                            
      DATA TWOPI /6.283185307179586D00/                                 
      DATA A /                                                          
     1+.82251280093D0,.99576620370D0,.03125246914D0,                    
     2 .97427079475D0,.71995354167D0,                                   
     3.036291645684716D0,.002737778519279D0,.036748195691688D0,         
     4 .033863192198393D0,-.000147094228332D0,                          
     5+1913865.D-20,-31233.D-20,-668609.D-20,-299023.D-20,+432630.D-20, 
     6 +8203.D-25,-1900.D-25,-190.D-25,+1077.D-25,+1266.D-25 /          
      DATA XL /+0.,+0.,+0.,+0.,+0.,+1.,+0.,+0.,+1.,+0.,+0.,+0.,+0./     
      DATA XLP/+0.,+0.,+0.,+0.,+1.,+0.,+1.,+0.,+0.,-1.,+0.,+2.,+2./     
      DATA XFX/+0.,+2.,+0.,+2.,+0.,+0.,+2.,+2.,+2.,+2.,+2.,+0.,+2./     
      DATA XD /+0.,-2.,+0.,+0.,+0.,+0.,-2.,+0.,+0.,-2.,-2.,+0.,-2./     
      DATA XN /+1.,+2.,+2.,+2.,+0.,+0.,+2.,+1.,+2.,+2.,+1.,+0.,+2./     
      DATA  SLC  /                                                      
     *-17.2327,-1.2729,+.2088,-.2037,+.1261,+.0675,-.0497,-.0342,-.0261,
     *+.0214,+.0124,+.0016,-.0015/                                      
      DATA  SLT  /                                                      
     *-.01737,-.00013,+.00002,-.00002,-.00031,+.00001,+.00012,-.00004,  
     *+.00000,-.00005,                                                  
     *+.00001,-.00001,+.00001/                                          
      DATA  COC  /                                                      
     *+ 9.2100,+0.5522,-.0904,+.0884,+.0000,+.0000,+.0216,+.0183,+.0113,
     *-.0093,-.0066,+.0000,+.0007/                                      
      DATA  COT  /                                                      
     *+.00091,-.00029,+.00004,-.00005,+.00000,+.00000,-.00006,+.00000,  
     *-.00001,+.00003,                                                  
     *+.00000,+.00000,+.00000/                                          
C     *****************************************************************
      T=XJD-2415020.0D0                                                 
      TC=T/36525.0D0                                                    
      DO 12 I=1,5                                                       
      FA(I)=DMOD(((A(I,4)*T+A(I,3))*T+A(I,2))*T,1.0D0)+A(I,1)           
      FA(I)=DMOD(FA(I),1.0D0)*TWOPI                                     
   12 SA(I)=FA(I)                                                       
      PL=0.0                                                            
      PO=0.0                                                            
      DO 15 I=1,13                                                      
      ARG=XL(I)*SA(1)+XLP(I)*SA(2)+XFX(I)*SA(3)+XD(I)*SA(4)+XN(I)*SA(5) 
      PO=PO+(COC(I)+COT(I)*TC)*COS(ARG)                                 
      PL=PL+(SLC(I)+SLT(I)*TC)*SIN(ARG)                                 
 15   CONTINUE                                                          
      XNUTL=PL*TWOPI/1296000.D0                                         
      XNUTO=PO*TWOPI/1296000.D0                                         
      RETURN                                                            
      END                                                               

c
c ----- END OF nutan.f ----- 
